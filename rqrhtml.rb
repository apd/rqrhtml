#!/usr/bin/env ruby
require 'rubygems'
require 'rqrcode' 

class RQRCode::QRCode 
def to_html
       res = []
       @modules.each_index do |col|
           tmp = []
           @modules.each_index do |row|
                if is_dark(col, row)
                    tmp << "            <td class = \"black\"></td>\r\n"
                else
                    tmp << "            <td class = \"white\"></td>\r\n"
                end
           end 
        res << tmp.join
       end 
       res.join("          </tr>\r\n          <tr>\r\n")
    end
end

begin 
	raise ArgumentError unless ARGV[0] 
	text = ARGV[0]

	ARGV[1].nil? ? htmlfile = "#{ENV['HOME']}/Desktop/qr.html" : htmlfile = ARGV[1]
	$outfile = File.open( htmlfile, 'w' ) 

	size = 12 
	level = 'h'

	title = "Here is the QR code you requested"

	qr = RQRCode::QRCode.new(text,  :size => size, :level => level)

	td_size = 3 
	table_width = qr.module_count.to_i * td_size

	$outfile.write( "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n") 
	$outfile.write( "<HTML>\r\n")
	$outfile.write( "<HEAD>\r\n")
	$outfile.write( "  <META http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\">\r\n") 
	$outfile.write( "  <META name=\"description\" content=\"That old QR C0de\">\r\n")
	$outfile.write( "  <STYLE type=\"text/css\">\r\n")
	$outfile.write( "    body { background-color: #123456; }\r\n") 
	$outfile.write( "    table.fg { border-width: 0; border-collapse: collapse; width: #{table_width}px; margin-left: auto; margin-right: auto; }\r\n")
	$outfile.write( "    table.bg { background-color: #ffffff; border-width: 0; width: #{table_width + 8 }px; height: #{table_width + 8}px; } \r\n" ) 
	$outfile.write( "    td { border-width: 0; border-style: none; border-collapse: collapse; padding: 0; margin: 0; width: #{td_size}px; height: #{td_size}px; }\r\n")
	$outfile.write( "    td.black { background-color:#000000;}\r\n")
	$outfile.write( "    td.white { background-color:#ffffff;}\r\n")
	$outfile.write( "  </STYLE>\r\n")
	$outfile.write( "  <TITLE>#{title}</TITLE>\r\n")
	$outfile.write( "</HEAD>\r\n")
	$outfile.write( "<BODY>\r\n") 
	$outfile.write( "  <TABLE class=\"bg\" summary=\"bg\">\r\n")
	$outfile.write( "    <TR>\r\n")
	$outfile.write( "      <TD>\r\n") 
	#$outfile.write( "        <center>") 
	$outfile.write( "        <TABLE class=\"fg\" summary=\"fg\">\r\n")
	$outfile.write( "          <TR>\r\n#{qr.to_html}") 
	$outfile.write( "          </TR>\r\n" )
	$outfile.write( "        </TABLE>\r\n") 
	#$outfile.write( "        </center>\r\n") 
	$outfile.write( "      </TD>\r\n")
	$outfile.write( "    </TR>\r\n")
	$outfile.write( "  </TABLE>\r\n")
	$outfile.write( "  <P>\r\n")
	$outfile.write( "    <A href=\"http://validator.w3.org/check?uri=referer\">\r\n" )
	$outfile.write( "      <IMG src=\"http://www.w3.org/Icons/valid-html401\" alt=\"Valid HTML 4.01 Transitional\" height=\"31\" width=\"88\"></A>\r\n") 
	$outfile.write( "   </P>\r\n") 
	$outfile.write( "</BODY>\r\n") 
	$outfile.write( "</HTML>") 

	$outfile.close

rescue ArgumentError => e
	print "Usage: #{File.basename($0)} 'Text to encode' [output file]"
rescue Exception => e 
	print "Caught #{e.to_s}"
end 
